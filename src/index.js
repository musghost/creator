import 'babel-polyfill';

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './app/containers/App';
import Resources from './app/containers/Resources';
import Planning from './app/containers/Planning';
import PlanningList from './app/containers/PlanningList';
import configureStore from './app/store/configureStore';
import {Router, Route, browserHistory, hashHistory} from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';

import 'flexboxgrid/css/flexboxgrid.css';
import 'font-awesome/css/font-awesome.css';
import './index.scss';

injectTapEventPlugin();

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Planning}/>
      <Route path="/planning" component={Resources}/>
      <Route path="/planningList" component={PlanningList} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
