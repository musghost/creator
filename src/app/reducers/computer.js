import {
  UPLOAD_FILE_FULFILLED
} from '../constants/ActionTypes';
import {API_BASE} from '../constants/config';

const initialState = {
  path: '',
  extension: ''
};

export default function computer(state = initialState, action) {
  switch (action.type) {
    case UPLOAD_FILE_FULFILLED:
      return {
        path: `${API_BASE}/${action.payload.data.path}`,
        extension: action.payload.data.extension.toLowerCase()
      };
    case 'RESTART_UPLOADING':
      return {
        path: '',
        extension: ''
      };
    default:
      return state;
  }
}
