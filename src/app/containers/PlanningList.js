import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as TodoActions from '../actions/index';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';

import SearchBox from '../components/SearchBox';
import Results from '../components/Results';
import Grid from '../components/Grid';
import Preview from '../components/Preview';
import Header from '../components/Header';
import Footer from '../components/Footer';

const style = {
  display: 'flex',
  justifyContent: 'flex-end'
}

class PlanningList extends Component {

  state = {
    open: false
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleSubmit = (values) => {
    const {actions, resources} = this.props;
    if(resources.type !== "desktop") {
      actions.fetchResources(resources.type, values.search);
    }
  }

  loadMore = () => {
    const {actions} = this.props;
    actions.loadMore();
  }

  updateTypeSearch = (type) => {
    this.props.actions.updateTypeSearch(type);
  }

  renderMoreResults() {
    const {resources} = this.props;
    if(resources.next) {
      return (
        <div>
          <button onClick={this.loadMore}>Cargar más</button>
        </div>
      );
    }
  }

  renderResults() {
    const {resources} = this.props;
    if(resources.loading) {
      return <div className="results"><img className="loading" src="assets/loading.gif" /></div>;
    } else {
      return (
        <Results results={resources.stream} type={resources.type}>
          {this.renderMoreResults()}
        </Results>);
    }
  }

  handleShowPreview = () => {
    const {actions} = this.props;
    actions.showPreview();
  }

  renderModals = () => {
    const {panel} = this.props;
    return (
      <div>
        {panel.showPreview ? <Preview/> : null}
      </div>
    );
  }

  render() {
    const {resources, actions} = this.props;
    return (
      <MuiThemeProvider>
        <div>
          <Header />
          <div className="wrapper-planning" style={{backgroundColor: '#fff', marginTop: '60px'}}>
            <div className="flex-planning">
              <div className="planning" style={{height: 'auto'}}>
                <h1>MIS PLANEACIONES</h1>
                <h2>Título</h2>
                <input type="text" placeholder='ej. "La monarquía y absolutismo"' />
                <h2>Año escolar</h2>
                <select>
                  <option>ej 1º Secundaria</option>
                </select>
                <h2>Materia</h2>
                <select>
                  <option>ej. Matemáticas</option>
                </select>
                <div>
                  <RaisedButton
                    className="filter-btn"
                    backgroundColor={'#777777'}
                    label="Filtrar planeación"
                    labelColor={'#FFF'}
                    labelStyle={{textTransform: 'camellcase'}}
                  />
                </div>
              </div>
              <div className="planning-list" style={{marginTop: '20px'}}>
                <div>
                  <RaisedButton
                    className="new-planning"
                    label="Nueva planeación"
                    labelColor={'#FFF'}
                    labelStyle={{textTransform: 'camellcase'}}
                    backgroundColor={'#A53B8B'}
                    onTouchTap={this.handleOpen}
                  />
                </div>
                <div className="table-planning">
                  <h1>PLANEACIONES RECIENTES</h1>
                  <table>
                    <th>Planeación</th>
                    <th>Editar</th>
                    <th>Enviar a CAP</th>
                    <th>Eliminar</th>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgTwo.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Título de la planeación Reflexión de la expresión corporal</h2>
                            <h3 className="user">Jhon Doe</h3>
                            <h4>Artes</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-purple" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-purple" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div className="hr"></div>
                <div className="table-planning">
                  <h1>PRIMARIA - 1ERO</h1>
                  <table>
                    <th>Planeación</th>
                    <th>Editar</th>
                    <th>Enviar a CAP</th>
                    <th>Eliminar</th>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgThree.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Organizar biblioteca del aula</h2>
                            <h4>Español</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgFour.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Uso del lenguaje para comunicarse</h2>
                            <h4>Español</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgFive.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Conocimiento y cuidado de sí mismo</h2>
                            <h4>Formación cívica y ética</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div className="table-planning">
                  <h1>PRIMARIA - 4TO</h1>
                  <table>
                    <th>Planeación</th>
                    <th>Editar</th>
                    <th>Enviar a CAP</th>
                    <th>Eliminar</th>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgSix.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>¿Cómo mejoro mi alimentación?</h2>
                            <h4>Ciencias Naturales</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgSeven.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Díselo a quien más confianza le tengas</h2>
                            <h4>Formación cívica y ética</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div className="planning-info">
                          <div>
                            <img src="../assets/imgEight.png" alt="logo-creator" className="img-logo" />
                          </div>
                          <div>
                            <h2>Deerecho de los niños a ser protegidos contra maltrato, abuso o explotación</h2>
                            <h4>Formación cívica y ética</h4>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-pencil-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-share-square-o icon-grey" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="center-anchor">
                          <a>
                            <i className="fa fa-trash icon-red" aria-hidden="true"></i>
                          </a>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </MuiThemeProvider>
    );
  }
}

PlanningList.propTypes = {
  resources: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    resources: state.resources,
    panel: state.panel
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanningList);
