import React, {PropTypes, Component} from 'react';
import Dropzone from 'react-dropzone';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import RaisedButton from 'material-ui/RaisedButton';
import * as TodoActions from '../actions/index';
import {FileResult} from '../components/itemResults';
import TextField from 'material-ui/TextField';

class Computer extends Component {

  state = {
    preview: null,
    haveFiles: false,
    extension: '',
    title: ''
  }

  onDrop = (files) => {
    const {actions} = this.props;
    actions.uploadFile(files);
    this.setState({
        preview: files[0].preview
    });
  }

  uploadFile = () => {
    const {actions} = this.props;
    actions.uploadImage(this.state.files);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.computer && nextProps.computer.extension) {
      this.setState({haveFiles: true, extension: nextProps.computer.extension});
    }
  }

  restartUploader = () => {
    this.props.actions.restartUploader();
    this.setState({
      preview: null,
      haveFiles: false,
      extension: '',
      title: ''
    });
  }

  render() {
    return (
      <div className="other">
        <h2>Archivos</h2>
        <div style={{display: (null !== this.state.preview) ? 'none' : ''}}>
          <TextField
            hintText="Título del recurso"
            onChange={(event, title) => this.setState({title})}
            />
          <Dropzone  onDrop={this.onDrop} multiple={false}>
            <p>Arrastra aquí tus archivos.</p>
          </Dropzone>
        </div>
        {!this.state.haveFiles ? null : (
          <div>
            <div className="image-preview">
              <FileResult result={{...this.props.computer, title: this.state.title, type: 'computer'}} />
            </div>
            <div>
              <button onClick={() => this.restartUploader()}>
                Regresar <i className="fa fa-refresh"></i>
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

Computer.propTypes = {
};

function mapStateToProps(state) {
  return {
    computer: state.computer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Computer);
