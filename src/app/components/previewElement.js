import React, {PropTypes, Component} from 'react';

class YouTube extends Component {
  render() {
    const {resource} = this.props;
    const videoID = resource.link.split('?v=')[1]
    return (
      <iframe src={`https://www.youtube.com/embed/${videoID}`} allowFullScreen></iframe>
    );
  }
}

YouTube.porpTypes = {
  resource: PropTypes.object
};

class Web extends Component {
  render() {
    const {resource} = this.props;
    return (
      <iframe src={resource.link}></iframe>
    );
  }
}

Web.porpTypes = {
  resource: PropTypes.object
};

class Text extends Component {

  createMarkup(markup) {
    return {__html: markup};
  }

  render() {
    const {resource} = this.props;
    return (
      <div className="text-frame" dangerouslySetInnerHTML={this.createMarkup(resource)}></div>
    );
  }
}

Text.porpTypes = {
  resource: PropTypes.object
};

class Computer extends Component {
  render() {
    const {resource} = this.props;
    let node;
    if(resource.extension.indexOf('png') >= 0 ||
      resource.extension.indexOf('jpg') >= 0 ||
      resource.extension.indexOf('jpeg') >= 0) {
      node = <img src={resource.path}/>;
    } else {
      node = (
        <div className="file-description">
          <h4>Descarga el archivo haciendo click sobre el ícono</h4>
          <a href={resource.path} target="_blank">
            <i className="fa fa-file-o file-icon" aria-hidden="true"/>
          </a>
        </div>
      );
    }
    return (
      <div className="file-frame">
        {node}
      </div>
    );
  }
}

export {
  YouTube, Web, Text, Computer
}
