import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as TodoActions from '../actions/index';

export const options = [
  ['Lenguaje y comunicación', 'Pensamiento matemático', 'Exploración y conocimiento del mundo', 'Desarrollo físico y salud', 'Desarrollo personal y social'],
  ['Lenguaje y comunicación', 'Pensamiento matemático', 'Exploración y conocimiento del mundo', 'Ingles', 'Desarrollo físico y salud', 'Desarrollo personal y social'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Geografía', 'Historia ', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Geografía', 'Historia ', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Exploración', 'Inglés', 'Formación Cívica y Ética', 'Geografía', 'Historia ', 'Educación Física', 'Educación Artística'],
  ['Español', 'Matemáticas', 'Biología', 'Geografía', 'Inglés', 'Historia (0)', 'FCyE (0)'],
  ['Español', 'Mamáticas', 'Física', 'FCyE I', 'Historia I', 'Inglés'],
  ['Español', 'Mamáticas', 'Química', 'FCyE II', 'Historia I', 'Inglés']
];

class Selectors extends Component {

  state = {
    year: 0
  }

  handleChange = (event) => {
    this.setState({year: event.target.value});
  }

  setModalVars = (event) => {
    this.props.actions.setResVars(this.state.year, event.target.value)
  }

  render() {
    return (
      <div>
        <h2>Año escolar</h2>
        <select onChange={this.handleChange}>
          <option value="0">1º Preescolar</option>
          <option value="0">2º Preescolar</option>
          <option value="1">3º Preescolar</option>
          <option value="2">1º Primaria</option>
          <option value="2">2º Primaria</option>
          <option value="2">3ª Primaria</option>
          <option value="2">4ª Primaria</option>
          <option value="2">5º Primaria</option>
          <option value="2">6º Primaria</option>
          <option value="8">1º Secundaria</option>
          <option value="9">2º Secundaria</option>
          <option value="10">3º Secundaria</option>
        </select>
        <h2>Materia</h2>
        <select value={this.state.value} onChange={this.setModalVars}>
          {options[this.state.year].map((option, opt) => {
            return <option  key={opt}>{option}</option>
          })}
        </select>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    resources: state.resources
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(TodoActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Selectors);
