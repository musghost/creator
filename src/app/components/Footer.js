import React, {PropTypes, Component} from 'react';
import classnames from 'classnames';
import {SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE} from '../constants/TodoFilters';

const FILTER_TITLES = {
  [SHOW_ALL]: 'All',
  [SHOW_ACTIVE]: 'Active',
  [SHOW_COMPLETED]: 'Completed'
};

class Footer extends Component {
  renderTodoCount() {
    const {activeCount} = this.props;
    const itemWord = activeCount === 1 ? 'item' : 'items';

    return (
      <span className="todo-count">
        <strong>{activeCount || 'No'}</strong> {itemWord} left
      </span>
    );
  }

  renderFilterLink(filter) {
    const title = FILTER_TITLES[filter];
    const {filter: selectedFilter, onShow} = this.props;
    const handleChange = () => onShow(filter);

    return (
      <a
        className={classnames({selected: filter === selectedFilter})}
        style={{cursor: 'pointer'}}
        onClick={handleChange}
        >
        {title}
      </a>
    );
  }

  renderClearButton() {
    const {completedCount, onClearCompleted} = this.props;
    if (completedCount > 0) {
      return (
        <button
          className="clear-completed"
          onClick={onClearCompleted}
          >
          Clear completed
        </button>
      );
    }
  }

  render() {
    return (
      <footer className="footer">
        <ul className="filters">
          <a href="#">
            <li>¿Quiénes somos?</li>
          </a>
          <a href="#">
            <li>Contáctanos</li>
          </a>
          <a href="#">
            <li>Política de privacidad</li>
          </a>
          <a href="#">
            <li>Términos y condiciones de uso</li>
          </a>
        </ul>
        <div>
          <a href="#">
            <i className="fa fa-facebook-square icon-social" aria-hidden="true"></i>
          </a>
          <a href="#">
            <i className="fa fa-twitter-square icon-social" aria-hidden="true"></i>
          </a>
          <a href="#">
            <i className="fa fa-google-plus-square icon-social" aria-hidden="true"></i>
          </a>
          <a href="#">
            <i className="fa fa-linkedin-square icon-social" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    );
  }
}

Footer.propTypes = {
  completedCount: PropTypes.number.isRequired,
  activeCount: PropTypes.number.isRequired,
  filter: PropTypes.string.isRequired,
  onClearCompleted: PropTypes.func.isRequired,
  onShow: PropTypes.func.isRequired
};

export default Footer;
